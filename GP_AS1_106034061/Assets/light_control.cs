﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class light_control : MonoBehaviour
{
    float duration = 1.0f;
    Color Red = Color.red;
    Color Blue = Color.blue;
    Color White=Color.white;
    string  state="Static";
    Light lt;
    // Start is called before the first frame update
    void Start()
    {
        lt = GetComponent<Light>();
    
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.C))
        {
            if(state =="Dynamic")
            {
                state="Static";
            }
            else if(state=="Static")
            {
                state="Dynamic";
            }
        }
        if(state=="Dynamic"){
            float t = Mathf.PingPong(Time.time, duration) / duration;
            lt.color = Color.Lerp(Color.white, Color.blue, t);

        }else if(state=="Static"){
            lt.color=White;

        }
        if(Input.GetKey(KeyCode.K)){
            lt.intensity+=(float)0.1;
        }
        if(Input.GetKey(KeyCode.L)){
            lt.intensity-=(float)0.1;
        }
    }
}
