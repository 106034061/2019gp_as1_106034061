﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerComplete : MonoBehaviour
{
    public GameObject Map;

    Rigidbody rb;
    Rigidbody m_rigid;
    [Header("角色的起始位置")]
    public Vector3 initPos;

    [Header("角色目前的位置")]
    public Vector3 curPos;
	private bool canJump;
    // Start is called before the first frame update
    public int blood=100;
    void Start()
    {
        m_rigid = this.gameObject.GetComponent<Rigidbody>();
        initPos = this.gameObject.transform.position;
        rb = GetComponent<Rigidbody>();
        blood=100;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Enemy")
            blood--;
		if(other.gameObject.tag == "Bonus"){
            Map.GetComponent<EventControllerComplete>().score++;
        }    
		if(other.gameObject.tag == "Life"){
            if(blood<100)
                blood++;
            
        }    
    }
    void FixedUpdate(){
                
        float moveVertical = Input.GetAxis ("Vertical");
        float speed = 3.0f;
        rb.velocity =  gameObject.transform.forward * speed * moveVertical + new Vector3(0, rb.velocity.y, 0);
        
        if(Input.GetKey(KeyCode.A)){
            gameObject.transform.Rotate(new Vector3(0, -2.0f, 0));
        }
        if(Input.GetKey(KeyCode.D)){
            gameObject.transform.Rotate(new Vector3(0, 2.0f, 0));
        }
        if(Input.GetKey(KeyCode.Space)){
            gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, 2.0f, 0)*10.0f);
        }

        if (Input.GetKeyDown(KeyCode.R)||
        (this.gameObject.transform.position.x<-1)||
        (this.gameObject.transform.position.x>101)||
        (this.gameObject.transform.position.y<-1)||
        (this.gameObject.transform.position.y>101))
            this.gameObject.transform.position = initPos;
        rb.angularVelocity = Vector3.zero;
    }
}
