﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventControllerComplete : MonoBehaviour
{
    public GameObject Cube;
    private GameObject curCube;
    public int score=0;
    // Start is called before the first frame update
    void Start()
    {
        score=0;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(curCube == null){
            float x = Random.Range(55f, 70f);
            float z = Random.Range(45f, 60f);
            // float x = 9;
            // float z = 9;
            curCube = Instantiate(Cube, new Vector3(x, 0.5f, z), Quaternion.identity);
            score++;
        }   
    }
}
