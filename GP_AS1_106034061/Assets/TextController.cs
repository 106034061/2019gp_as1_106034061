﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TextController : MonoBehaviour
{
    // public int score=0;
    public GameObject Map;
    public GameObject Player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Text>().text = "Score:"+Map.GetComponent<EventControllerComplete>().score +"\n"+"Blood:"+Player.GetComponent<PlayerControllerComplete>().blood;   

    }
}
